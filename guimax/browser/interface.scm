;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax browser interface)
  #:use-module (srfi srfi-9)
  #:use-module (emacsy window)
  #:export (<user-data>
            current-view
            user-data?
            make-user-data
            user-data-widget
            user-data-window
            user-data-view
            set-user-data-view
            user-data-mode-line))

(define-record-type <user-data>
  (make-user-data widget window view mode-line)
  user-data?
  (widget user-data-widget)
  (window user-data-window)             ; GI: avoid `children'
  (view user-data-view set-user-data-view)
  (mode-line user-data-mode-line))

(define (current-view)
  (let ((ui-data (user-data current-window)))
    (and (user-data? ui-data)
         (user-data-view ui-data))))

(define noop (lambda _ #t))

(define-public ui 'none)

(define-public idle noop)
(define-public init noop)
(define-public key-event noop)

(define-public run noop)
(define-public quit noop)
(define-public draw-minibuffer noop)
(define-public draw-mode-line noop)

(define-public make-buffer-window noop)
(define-public make-box noop)
(define-public set-root-window! noop)

(define-public set-view! noop)

(define-public make-text-view noop)
(define-public text-view-set-text noop)

(define-public make-web-view noop)
(define-public get-url noop)
(define-public set-url! noop)
(define-public web-view-back-url noop)
(define-public web-view-next-url noop)
(define-public web-view-find-next noop)
(define-public web-view-find-previous noop)
(define-public web-view-find-quit noop)
(define-public web-view-reload noop)
