;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax browser guile-gnome)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome glib)
  #:use-module (gnome gobject)
  #:use-module (gnome gtk)
  #:use-module (gnome gtk gdk-event)
  #:use-module (gnome pango)
  #:use-module (gnome webkit)
  #:use-module (guimax browser interface))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle browser))
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (let* ((keyval (gdk-event-key:keyval event))
         (gtk-modifiers (gdk-event-key:modifiers event))
         (unicode (gdk-keyval-to-unicode keyval))
         (name (gdk-keyval-name keyval)))
    (%debug "key-press val=~s, name=~s, gtk-modifiers=~s\n" keyval name gtk-modifiers)
    (let* ((gdk-key-name->command-char-alist `(("Tab" . #\tab)
                                               ("Return" . #\return)
                                               ("Delete" . #\delete)
                                               ("BackSpace" . #\backspace)
                                               ("Escape" . #\escape)
                                               ("Left" . #\x80)
                                               ("Up" . #\x81)
                                               ("Right" . #\x82)
                                               ("Down" . #\x83)))
           (char (if (not (zero? unicode)) (integer->char unicode)
                     (assoc-ref gdk-key-name->command-char-alist name)))
           (gtk-modifiers->emacsy-alist `((control-mask . control)
                                          (mod1-mask . meta)
                                          (mod4-mask . super)
                                          (shift-mask . shift)))
           (modifiers (map (cut assoc-ref gtk-modifiers->emacsy-alist <>) gtk-modifiers)))
      (%debug "key-press char=~s modifiers=~s\n" char modifiers)
      (and (char? char)
           (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                  (#\delete #\d (control ,@modifiers))
                                                  (#\x80 #\b (control))
                                                  (#\x81 #\p (control))
                                                  (#\x82 #\f (control))
                                                  (#\x83 #\n (control)))
                                                char)
                                     `(,char ,modifiers))))
             ;; (apply (module-ref hello 'key-event) char+modifiers)
             (apply key-event char+modifiers))))))

(define (initialize frame browser options)
  (let* ((vbox (make <gtk-vbox>))
         (minibuffer (make <gtk-label> #:label ""))
         (root-container (make <gtk-vbox>))
         (root-window (make <gtk-vbox>)))

    (modify-bg frame  'normal "white")
    (modify-font minibuffer (pango-font-description-from-string "monospace 30"))
    (set-alignment minibuffer 0 0)

    (add frame vbox)

    (pack-start vbox root-container #t #t 0)
    (pack-start vbox minibuffer #f #f 0)
    ;;(add root-container root-window)
    (pack-start root-container root-window #t #t 0)

    (connect frame 'key-press-event (gtk-key-press browser))
    (g-timeout-add 200 (gtk-idle browser))

    (show-all frame)

    (module-define! browser 'quit (lambda _ (gtk-main-quit)))
    (module-define! browser 'draw-mode-line (lambda (mode-line text selected?)
                                              (let* ((bg (if selected? "darkgrey" "lightgrey"))
                                                     (markup (format #f "<span foreground=\"black\" background=\"~a\" underline=\"none\"><tt>~a</tt></span>" bg text))
                                                     (ebox (get-parent mode-line)))
                                                (set-markup mode-line markup)
                                                (modify-bg ebox 'normal bg)
                                                (show mode-line))))
    (module-define! browser 'draw-minibuffer (lambda (x) (set-text minibuffer x)))

    (module-define! browser 'make-buffer-window
                    (lambda (view)
                      (let ((vbox (make <gtk-vbox>))
                            (ebox (make <gtk-event-box>))
                            (mode-line (make <gtk-label> #:label " :--- guimax"))
                            (window (make <gtk-scrolled-window>))
                            (v (if (is-a? view <webkit-web-view>) (make-web-view)
                                   (make-text-view))))

                        (set-policy window 'automatic 'automatic)
                        (set-line-wrap mode-line #t)
                        (modify-bg ebox 'normal "lightgray")

                        (when (and view
                                   (is-a? v <gtk-text-view>))
                          (set-buffer v (get-buffer view)))

                        (when (and view
                                   (is-a? v <webkit-web-view>))
                          (set-url! v (get-url view)))

                        (modify-font mode-line (pango-font-description-from-string "monospace 30"))
                        (set-alignment mode-line 0 0)

                        (pack-start vbox window #t #t 0)
                        (pack-start vbox ebox #f #f 0)
                        (add ebox mode-line)
                        (add window v)

                        (make-user-data vbox window view mode-line))))

    (module-define! browser 'make-box
                    (lambda (children orientation)
                      (let ((box (make (if (eq? orientation 'vertical) <gtk-vbox> <gtk-hbox>))))
                        (set-homogeneous box #t)
                        (for-each
                         (lambda (ui-data)
                           (let* ((w (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                                  (parent (get-parent w)))
                             (when parent
                               (remove parent w))
                             (pack-start box w #t #t 0)))
                         children)
                        (show-all box)
                        box)))

    (module-define! browser 'set-root-window!
                    (lambda (ui-data)
                      (when root-window
                        (remove root-container root-window))
                      (set! root-window (if (user-data? ui-data) (user-data-widget ui-data) ui-data))
                      (pack-start root-container root-window #t #t 0)
                      (show-all root-container)))

    (module-define! browser 'set-view!
                    (lambda (u v)
                      (let ((view (user-data-view u)))
                        (when (and (is-a? view <webkit-web-view>)
                                   (or (is-a? v <gtk-text-view>)
                                       (string? v)))
                          (let* ((box (user-data-widget u))
                                 (children (get-children box))
                                 (window (car children))
                                 (old (get-child window))
                                 (v (make-text-view)))
                            (when old
                              (remove window old))
                            (add window v)
                            (set-user-data-view u v)
                            (set! view v)
                            (show-all box)))
                        (cond ((is-a? v <gtk-text-view>)
                               (let ((buffer (get-buffer v)))
                                 (set-buffer view buffer)))
                              ((is-a? v <webkit-web-view>)
                               (let* ((box (user-data-widget u))
                                      (children (get-children box))
                                      (window (car children))
                                      (old (get-child window)))
                                 (when old
                                   (remove window old))
                                 ;; Web views cannot be duplicated, cater for split windows.
                                 (let ((v-new (make-web-view)))
                                   (set-user-data-view u v-new)
                                   (set-url! v-new (get-url v))
                                   (add window v-new))
                                 (show-all box)))
                              ((and (is-a? view <gtk-text-view>) (string? v))
                               (let ((buffer (get-buffer view)))
                                 (set-text buffer v)))))))

    (let ((url #f)
          (%search-text #f)
          (*cursor* #f)
          (*cursor-add?* #f))

      (define (init-text-view view)
        (set-cursor-visible view #t)
        (set-wrap-mode view 'char)
        (modify-font view (pango-font-description-from-string "monospace 20"))
        view)

      (define (init-search! view x)
        (when (not (equal? x %search-text))
          (set-highlight-text-matches view #t)
          (set! %search-text x)))

      (module-define! browser 'make-text-view (lambda _ (init-text-view (make <gtk-text-view>))))

      (module-define! browser 'text-view-set-text
                      (lambda (view text)
                        (set-text (get-buffer view) text)))

      (module-define! browser 'make-web-view (lambda _ (make <webkit-web-view>)))
      (module-define! browser 'get-url
                      (lambda (view)
                        (if (not view) "about:blank"
                            (let ((uri (get-uri view)))
                              (when (and uri (not (equal? uri ""))
                                         (not (equal? uri "about:blank")))
                                (set! url uri))
                              url))))
      (module-define! browser 'set-url! (lambda (view x) (set! url x) (load-uri view url)))

      (module-define! browser 'web-view-back-url (lambda (view) (go-back view)))
      (module-define! browser 'web-view-next-url (lambda (view) (go-forward view)))
      (module-define! browser 'web-view-reload (lambda (view) (reload view)))

      (module-define! browser 'web-view-find-next (lambda (view x) (init-search! view x) (search-text view %search-text #f #t #t)))
      (module-define! browser 'web-view-find-previous (lambda (view x) (init-search! view x) (search-text view %search-text #f #f #t)))
      (module-define! browser 'web-view-find-quit
                      (lambda (view)
                        (and %search-text
                             (set-highlight-text-matches view #f)
                             (set! %search-text #f)))))
    (init options)))

(define (run browser options)
  (let ((app (make <gtk-window> #:type 'toplevel)))
    (initialize app browser options)
    (gtk-main)))

(module-define! (resolve-module '(guimax browser interface)) 'run run)
