;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2013 Shane Celis <shane.celis@gmail.com>
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax browser web)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (emacsy emacsy)
  #:use-module (emacsy buffer)
  #:use-module (emacsy window)
  #:use-module (guimax browser interface)
  #:export (<web-view-buffer> web-view-buffer-view))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define (browse-url urlish)
  (set-buffer-name! urlish)
  (cond
   ((string=? urlish "home") (set-url! (current-view) "http://gnu.org"))
   ((string=? urlish "search") (set-url! (current-view) "http://duckduckgo.org"))
   ((string-prefix? "http://" urlish) (set-url! (current-view) urlish))
   ((string-contains urlish " ")
    ;; It contains spaces.  It's probably a search.
    (set-url!
     (current-view)
     (format #f "http://duckduckgo.org/search?q=~a"
             (string-map (lambda (c) (if (eq? c #\space) #\+ c)) urlish))))
   ((string-contains urlish ".")
    (set-url! (current-view) (format #f "http://~a" urlish)))
   (else
    ;; It's just one word.  Let's try adding a .org and http:// if it
    ;; needs it.
    (set-url! (current-view) (format #f "http://~a.org" urlish)))))

(define-interactive
  (browser #:optional
           (urlish (read-from-minibuffer "Enter URL or keywords: ")))
  "visit URLISH in current web buffer"  ; Try: C-h c browser RET

  (if (is-a? (current-buffer) <web-view-buffer>) (browse-url urlish)
      (new-tab urlish)))

(define-interactive (back-url)
  (web-view-back-url (current-view)))

(define-interactive (next-url)
  (web-view-next-url (current-view)))

(define-interactive (reload)
  (if (get-url (current-view)) (web-view-reload (current-view))
      (browser)))

(define %search-text #f)

;; These aren't as good as Emacs' isearch-forward, but they're not
;; a bad start.
(define-interactive
  (search-forward #:optional
                   (text (or %search-text (read-from-minibuffer "Search: "))))
  (set! %search-text text)
  (web-view-find-next text (current-view)))

(define-interactive
  (search-backward #:optional
                  (text (or %search-text (read-from-minibuffer "Search: "))))
  (set! %search-text text)
  (web-view-find-previous (current-view) text))

(define-interactive (browser-quit)
  (set! %search-text #f)
  (unless (web-view-find-quit (current-view))
    (keyboard-quit)))

(define-interactive (toggle-input-mode)
  (set! (buffer-modes (current-buffer))
        (if (equal? (buffer-modes (current-buffer)) `(,web-mode)) `(,char-mode)
            `(,web-mode))))

(define web-map
  (let ((keymap (make-keymap global-map)))
    (for-each
     (match-lambda ((key command) (define-key keymap key command)))
     `(("C-g" browser-quit)
       ("g" reload)
       ("G" browser)
       ("C-s" search-forward)
       ("C-r" search-backward)
       ("f" next-url)
       ("l" back-url)
       ("C-z" toggle-input-mode)))
    keymap))

(define web-mode (make <mode> #:mode-name "web" #:mode-map web-map))

(define char-map
  (let ((keymap (make-keymap)))
    (for-each
     (match-lambda ((key command) (define-key keymap key command)))
     `(("C-z" toggle-input-mode)
       ("C-x C-c" quit-application)
       ("M-x" execute-extended-command)))
    keymap))

(define char-mode (make <mode> #:mode-name "char" #:mode-map char-map))

(define-class <web-view-buffer> (<buffer>)
  (view #:accessor buffer-view #:init-form (make-web-view)))

(define web-view-buffer-view buffer-view)

(define-interactive (new-tab #:optional (urlish "search"))
  (define (on-enter)
    (format (current-error-port) "Setting web-view to ~a~%" (buffer-view (current-buffer)))
    (let* ((ui-data (user-data (selected-window)))
           (buffer (current-buffer))
           (view (buffer-view buffer))
           (url (get-url view)))
      (unless url
        (set-url! view urlish))
      (when ui-data
        (set-view! ui-data (buffer-view buffer))
        (set-user-data-view ui-data (buffer-view buffer)))))
  (let ((buffer (make <web-view-buffer> #:name urlish #:buffer-modes `(,web-mode))))
    (add-buffer! buffer)
    ;; FIXME: C-x o refreshes buffer
    (set! (buffer-modified-tick buffer) (current-time))
    (switch-to-buffer buffer)
    (add-hook! (buffer-enter-hook buffer) on-enter)
    (on-enter)
    (browser urlish)))
