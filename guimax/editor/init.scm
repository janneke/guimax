;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor init)
  #:use-module (ice-9 getopt-long)
  #:use-module (system repl server)
  #:use-module (oop goops)
  #:use-module (emacsy emacsy)
  #:use-module (emacsy buffer)
  #:use-module (emacsy text)
  #:use-module (emacsy core)
  #:use-module (guimax editor interface)
  #:use-module (guimax editor modes)
  #:use-module (guimax editor text)
  #:use-module (guimax editor web)

  #:use-module (emacsy window)
  #:use-module (guimax editor window)

  #:export (idle init key-event)
  #:re-export (messages))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define (init options)
  (set! current-window (make <ui-window> #:window-buffer messages))
  (set! root-window (make <internal-window> #:window-children (list current-window)))

  (let ((text (with-buffer messages (buffer-string))))
    (remove-buffer! messages)
    (set! messages (new-buffer (buffer-name messages)))
    (set! (buffer-modes messages) '())
    (with-buffer messages (insert text)))

  (let ((text (with-buffer scratch (buffer-string))))
    (remove-buffer! scratch)
    (set! scratch (new-buffer))
    (set! (buffer-modes scratch) `(,guile-interaction-mode))
    (with-buffer scratch (insert text)))

  (let ((files (option-ref options '() '())))
    (format (current-error-port) "files: ~s\n" files)
    (map find-file files))

  (set-root-window! (instantiate-root-window)))

(define (idle)
  (%debug "idle\n")
  (emacsy-tick)
  (when emacsy-quit-application?
    (quit))
  (draw-minibuffer (emacsy-message-or-echo-area))
  (redisplay-windows)
  #t)

(define (key-event char modifiers)
  (%debug "char: ~s, modifiers=~s\n" char modifiers)
  (and (not (eq? char #\nul))
       (emacsy-key-event char modifiers)
       (not emacsy-ran-undefined-command?)))
