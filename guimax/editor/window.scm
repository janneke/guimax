;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2013 Shane Celis <shane.celis@gmail.com>
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor window)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (oop goops)
  #:use-module (emacsy emacsy)
  #:use-module (emacsy buffer)
  #:use-module (emacsy text)
  #:use-module (emacsy minibuffer)
  #:use-module (emacsy core)
  #:use-module (emacsy window)
  #:use-module (guimax editor text)
  #:use-module (guimax editor web)
  #:use-module (guimax editor interface)
  #:export (<ui-window>
            instantiate-window
            instantiate-root-window
            redisplay
            redisplay-windows))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define-class <ui-window> (<window>)
  (last-buffer-modified-tick #:accessor last-buffer-modified-tick #:init-value -1))

(define-method (needs-redisplay? (window <ui-window>))
  (let* ((buffer (window-buffer window))
         (buffer-tick (buffer-modified-tick buffer))
         (window-tick (last-buffer-modified-tick window)))
    (not (= buffer-tick window-tick))))

(define-method (redisplayed! (window <ui-window>))
  (let* ((buffer (window-buffer window))
         (buffer-tick (buffer-modified-tick buffer)))
    (set! (last-buffer-modified-tick window) buffer-tick)))

(define-method (window-clone (window <ui-window>))
  (let ((new-window (next-method)))
    (set! (user-data new-window) #f)
    (set! (last-buffer-modified-tick new-window) -1)
    new-window))

(define (ui-after-change buffer)
  (set! (local-var 'needs-redisplay?) #t))

(add-hook! after-buffer-change-hook ui-after-change)

(define-method (redisplay (window <window>))
  (let ((buffer (window-buffer window))
        (ui-data (user-data window)))

    (when (and buffer ui-data (user-data? ui-data)
               (not emacsy-display-minibuffer?))
      (%debug "redisplaying window ~a with buffer ~a~a~%" window ui-data buffer)
      (draw-mode-line (user-data-mode-line ui-data) (emacsy-mode-line buffer) (eq? window (selected-window)))

      (when (or (needs-redisplay? window)
                (eq? window (selected-window)))
        (cond ((is-a? buffer <text-view-buffer>)
               (set-view! ui-data (text-view-buffer-view (current-buffer)) (eq? window (selected-window))))
              ((is-a? buffer <text-buffer>)
               (set-view! ui-data (buffer:buffer-string buffer) (eq? window (selected-window))))
              ((is-a? buffer <web-view-buffer>)
               (set-view! ui-data (web-view-buffer-view (current-buffer)) (eq? window (selected-window)))))
        (redisplayed! window)))))

(define-method (redisplay (window <internal-window>))
  (for-each redisplay (window-children window)))

(define (redisplay-windows)
  (redisplay root-window))

(define-interactive (test-window-change)
  (ui-window-configuration-change #f))

(define (ui-window-configuration-change internal-window)
  (set-root-window! (instantiate-root-window))
  (set-view! (user-data (selected-window)) (text-view-buffer-view (current-buffer)) #t))

(define (instantiate-root-window)
  (instantiate-window root-window))

(add-hook! window-configuration-change-hook ui-window-configuration-change)

(define-method (instantiate-window (window <window>))
  (let ((buffer (window-buffer window)))
    (instantiate-window window buffer)))

(define-method (instantiate-window (window <window>) (buffer <text-buffer>))
  (or (user-data window)
      (let ((ui-data (make-buffer-window (make-text-view))))
        (set! (user-data window) ui-data)
        (set-view! ui-data (buffer:buffer-string buffer) (eq? window (selected-window)))
        ui-data)))

(define-method (instantiate-window (window <window>) (buffer <text-view-buffer>))
  (or (user-data window)
      (let ((ui-data (make-buffer-window (make-text-view))))
        (set! (user-data window) ui-data)
        (set-view! ui-data (text-view-buffer-view buffer) (eq? window (selected-window)))
        ui-data)))

(define-method (instantiate-window (window <window>) (buffer <web-view-buffer>))
  (or (user-data window)
      (let ((ui-data (make-buffer-window (make-web-view))))
        (set! (user-data window) ui-data)
        ui-data)))

(define-method (instantiate-window (window <internal-window>))
  (make-box (map instantiate-window (window-children window)) (orientation window)))

(let ((old-func kill-buffer))
  (set! kill-buffer
        (lambda-cmd args
                    (let ((result (apply old-func args)))
                      (format #t "Killing buffer ~a~%" (current-buffer))
                      (set! (window-buffer current-window) (current-buffer))
                      result))))
