// Guimax --- Guile UI with Emacsy
// Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
// Copyright (C) 2019 Rutger EW van Beusekom <rutger.van.beusekom@gmail.com>
//
// This file is part of Guimax.
//
// Guimax is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Guimax is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

#include <wx/wx.h>
#include <wx/stc/stc.h>
#include <libguile.h>

struct Text_view: public wxStyledTextCtrl
{
  int mark_offset;
  Text_view (wxWindow* parent, int foo)
    : wxStyledTextCtrl (parent, foo)
    , mark_offset ()
  {
    this->Bind (wxEVT_KEY_DOWN,
                [&] (wxKeyEvent& e)
                {
                  if (!scm_is_true (scm_call_2 (scm_c_private_ref ("guimax editor wx", "wx-key-press"),
                                                scm_from_int (e.GetKeyCode ()),
                                                scm_from_int (e.GetModifiers ()))))
                    e.Skip ();
                });

    //fprintf (stderr, "scale: %f\n", wxWindow::GetContentScaleFactor ());
    this->SetUseHorizontalScrollBar (true);
    this->SetUseVerticalScrollBar (true);
    this->SetLexer(wxSTC_LEX_CPP);

    wxFont normal(30,wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL);
    wxFont italic(30,wxFONTFAMILY_TELETYPE,wxFONTSTYLE_ITALIC,wxFONTWEIGHT_NORMAL);
    wxFont bold(30,wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD);

    wxColor white(225,255,255);
    wxColor comment_green(60, 160, 60);
    wxColor comment_orange(150, 50, 50);
    wxColor green(0,100,0);
    wxColor red(225, 100,100);
    wxColor blue(0, 0, 200);
    wxColor black(0, 0, 0);
    wxColor turqoise(0, 255, 255);
    wxColor magenta(255, 0, 255);

    // Set the color to use for various elements
    this->StyleSetFont(wxSTC_STYLE_DEFAULT, normal);
    this->StyleSetFont(wxSTC_C_COMMENTLINE, italic);
    this->StyleSetFont(wxSTC_C_COMMENT, italic);
    this->StyleSetFont(wxSTC_C_PREPROCESSOR, normal);
    this->StyleSetFont(wxSTC_C_STRING, normal);
    this->StyleSetFont(wxSTC_C_OPERATOR, normal);

    this->StyleSetFont(wxSTC_STYLE_BRACELIGHT, normal);
    this->StyleSetBackground(wxSTC_STYLE_BRACELIGHT, turqoise);

    this->StyleSetFont(wxSTC_STYLE_BRACEBAD, normal);
    this->StyleSetBackground(wxSTC_STYLE_BRACEBAD, magenta);

    this->StyleSetForeground(wxSTC_C_COMMENTLINE, comment_green);
    this->StyleSetForeground(wxSTC_C_COMMENT, comment_orange);
    this->StyleSetForeground(wxSTC_C_OPERATOR, black);

    this->StyleSetFont(wxSTC_C_WORD,normal);
    this->StyleSetForeground(wxSTC_C_WORD, blue);
    this->SetKeyWords(0,"interface component provides requires behaviour system in out");

    this->StyleSetFont(wxSTC_C_WORD2,normal);
    this->StyleSetForeground(wxSTC_C_WORD2, red);
    this->SetKeyWords(1,"on inevitable optional illegal void bool true false enum subint if else");

    this->SetCaretStyle(wxSTC_CARETSTYLE_BLOCK);

    this->SetIndent(2);
    this->SetUseTabs(false);

    this->SetMarginType(0, wxSTC_MARGIN_NUMBER);
    this->SetMarginWidth(0,0);
    this->SetMarginWidth(1,0);
    this->SetMarginWidth(2,0);
  }

  virtual void DeleteRange (int offset, int length)
  {
    wxStyledTextCtrl::DeleteRange (offset, length);
    if (this->mark_offset > offset + length)
      this->mark_offset -= length;
    else if (this->mark_offset > offset)
      this->mark_offset = offset;
  }

  virtual void SetText (wxString const& string)
  {
    wxStyledTextCtrl::SetText (string);
    this->mark_offset = 0;
  }

  virtual void WriteText (wxString const& string)
  {
    wxStyledTextCtrl::WriteText (string);
    if (this->mark_offset < this->GetCurrentPos ())
      this->mark_offset += string.Length ();
  }
  // "Flicker-Free Drawing (TM)"
  virtual void OnEraseBackGround (wxEraseEvent& event) {}
  DECLARE_EVENT_TABLE ()
};

BEGIN_EVENT_TABLE (Text_view, wxStyledTextCtrl)
  EVT_ERASE_BACKGROUND (Text_view::OnEraseBackGround)
END_EVENT_TABLE()

class Frame: public wxFrame
{
public:
  Frame (wxString const& title, wxPoint const& pos, wxSize const& size);
private:
  void OnHello (wxCommandEvent& event);
  void OnExit (wxCommandEvent& event);
  void OnAbout (wxCommandEvent& event);
  wxDECLARE_EVENT_TABLE ();
};
enum
  {
   ID_Hello = 1
  };

wxBEGIN_EVENT_TABLE (Frame, wxFrame)
EVT_MENU (ID_Hello,   Frame::OnHello)
EVT_MENU (wxID_EXIT,  Frame::OnExit)
EVT_MENU (wxID_ABOUT, Frame::OnAbout)
wxEND_EVENT_TABLE ()

wxApp *app;
Frame *frame;
Frame *orphanage;
wxBoxSizer* top_vbox = 0;
wxStaticText* minibuffer = 0;
wxBoxSizer* root_container = 0;
wxBoxSizer* root_window = 0;

Frame::Frame (wxString const& title, wxPoint const& pos, wxSize const& size)
  : wxFrame (NULL, wxID_ANY, title, pos, size)
{
  wxMenu *menuFile = new wxMenu;
  menuFile->Append (ID_Hello, "&Hello...\tCtrl-H",
                    "Help string shown in status bar for this menu item");
  menuFile->AppendSeparator ();
  menuFile->Append (wxID_EXIT);
  wxMenu *menuHelp = new wxMenu;
  menuHelp->Append (wxID_ABOUT);
  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append (menuFile, "&File");
  menuBar->Append (menuHelp, "&Help");
  SetMenuBar (menuBar);
  // CreateStatusBar ();
  // SetStatusText ("Welcome to wxWidgets!");

  auto frame = this;

  wxBoxSizer *vbox = new wxBoxSizer (wxVERTICAL);
  top_vbox = vbox;
  minibuffer = new wxStaticText (frame, wxID_ANY, "");
  root_container = new wxBoxSizer (wxVERTICAL);
  //root_window = new wxBoxSizer (wxVERTICAL);
  minibuffer->SetLabelMarkup ("Quit!");

  vbox->Add (root_container, 1, wxEXPAND);
  vbox->Add (minibuffer);
}

void
Frame::OnExit (wxCommandEvent& event)
{
  Close (true);
}

void
Frame::OnAbout (wxCommandEvent& event)
{
  wxMessageBox ("This is a wxWidgets' Hello world sample",
                "About Hello World", wxOK | wxICON_INFORMATION);
}

void
Frame::OnHello (wxCommandEvent& event)
{
  wxLogMessage ("Hello world from wxWidgets!");
}

SCM_DEFINE (scm_run, "run", 2, 0, 0,
            (SCM editor, SCM options),
            "")
{
  //fprintf (stderr, "wx: run\n");
  int argc;
  char *argv[] = {};

  wxApp* app = new wxApp ();
  wxEntryStart (argc, argv);
  if (app->OnInit ())
    {
      frame = new Frame ("Hello World", wxPoint (50, 50), wxSize (450, 340));
      orphanage = new Frame ("Orphanage", wxPoint (0, 0), wxSize (0, 0));

      app->Bind (wxEVT_KEY_DOWN,
            [&] (wxKeyEvent& e)
            {
              if (!scm_is_true (scm_call_2 (scm_c_private_ref ("guimax editor wx", "wx-key-press"),
                                            scm_from_int (e.GetKeyCode ()),
                                            scm_from_int (e.GetModifiers ()))))
                e.Skip ();
            });

      wxTimer idle;
      idle.Bind (wxEVT_TIMER,
                 [&] (wxTimerEvent& e)
                 {
                   scm_call_0 (scm_c_private_ref ("guimax editor wx", "wx-idle"));
                 });
      idle.Start (100);

      scm_call_2 (scm_c_private_ref ("guimax editor wx", "wx-init"), editor, options);
      app->OnRun ();
    }

  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_quit, "quit", 0, 0, 0,
           (),
           "")
{
  //fprintf (stderr, "wx: quit\n");
  //frame->Close ();
  exit (0);
}

SCM_DEFINE (scm_draw_mode_line, "draw-mode-line", 3, 0, 0,
            (SCM mode_line, SCM text, SCM selected_p),
           "")
{
  char markup[256];
  sprintf (markup, "<span foreground=\"black\" background=\"%s\" underline=\"none\"><tt>%s</tt></span>",
           scm_is_true (selected_p) ?  "darkgrey" : "lightgrey",
           scm_to_utf8_string (text));
  wxStaticText *w = static_cast<wxStaticText*> (scm_to_pointer (mode_line));
  w->SetLabelMarkup (markup);
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_draw_minibuffer, "draw-minibuffer", 1, 0, 0,
            (SCM text),
           "")
{
  minibuffer->SetLabelMarkup (scm_to_utf8_string (text));
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_make_text_view, "make-text-view", 0, 0, 0,
           (),
           "")
{
  Text_view* v = new Text_view (orphanage, wxID_ANY);
  // fprintf (stderr, "make-text-view view=0x%x\n", v);
  v->Hide ();
  return scm_from_pointer (v, 0);
}

SCM_DEFINE (scm_set_view_x, "set-view!", 3, 0, 0,
            (SCM ui_data, SCM scm_view, SCM selected_p),
            "")
{
  Text_view *v = static_cast<Text_view*> (scm_to_pointer (scm_view));
  //fprintf (stderr, "set-view! view=0x%x\n", v);
  SCM user_data = scm_c_eval_string ("<user-data>");
  if (scm_is_true (scm_struct_p (user_data))
      && scm_is_true (scm_equal_p (scm_struct_vtable (ui_data), user_data)))
    {
      Text_view *view = static_cast<Text_view*>
        (scm_to_pointer (scm_struct_ref (ui_data, scm_from_int (2))));
      wxBoxSizer *box = static_cast<wxBoxSizer*>
        (scm_to_pointer (scm_struct_ref (ui_data, scm_from_int (0))));
      wxSizerItemList& children = box->GetChildren ();
      wxSizerItem *first = children.GetFirst ()->GetData ();
      wxWindow *first_window = first->GetWindow ();
      wxStyledTextCtrl *window_view = dynamic_cast<wxStyledTextCtrl*> (first_window);
      void *buffer = v->GetDocPointer ();
      if (buffer != window_view->GetDocPointer ())
        {
          view->SetDocPointer (buffer);
          window_view->SetDocPointer (buffer);
        }
      if (window_view->GetCurrentPos () != v->GetCurrentPos ())
        window_view->GotoPos (v->GetCurrentPos ());

      if (window_view->GetAnchor () != v->GetAnchor ())
        window_view->SetAnchor (v->GetAnchor ());

      if (scm_is_true (selected_p) && !window_view->HasFocus ())
        {
          window_view->Show ();
          window_view->SetFocus ();
        }
    }
    
  return SCM_UNSPECIFIED;
}

void set_root_window (wxBoxSizer* r);

SCM_DEFINE (scm_make_buffer_window, "make-buffer-window", 1, 0, 0,
           (SCM view),
           "")
{
  wxBoxSizer* vbox = new wxBoxSizer (wxVERTICAL);
  // fprintf (stderr, "buffer-window: 0x%x\n", vbox);

  auto frame = static_cast<wxApp*> (wxApp::GetInstance ())->GetTopWindow ();
  wxStaticText* mode_line = new wxStaticText (frame, wxID_ANY, "");
  mode_line->SetBackgroundColour (wxColor ("darkgrey"));
  mode_line->SetLabelMarkup ("<span foreground=\"black\" background=\"darkgrey\"> --:- guimax</span>");

  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  wxWindow *parent = v->GetParent ();
  if (v->GetParent () == orphanage)
    v->Reparent (frame);
  else
    {
      Text_view* buffer_v = new Text_view (frame, wxID_ANY);
      buffer_v->SetDocPointer (v->GetDocPointer ());
      v = buffer_v;
    }

  vbox->Add (v, 1, wxEXPAND);
  vbox->Add (mode_line);
  vbox->Show (true);

  if (!root_window)
    set_root_window (vbox);

  SCM user_data = scm_make_struct_no_tail (scm_c_eval_string ("<user-data>"),
                                           scm_list_4 (scm_from_pointer (vbox, 0),
                                                       scm_from_pointer (0, 0),
                                                       scm_from_pointer (v, 0),
                                                       scm_from_pointer (mode_line, 0)));

  return user_data;
}

SCM_DEFINE (scm_get_wx_widget, "get-wx-widget", 1, 0, 0,
            (SCM pointer),
            "")
{
  SCM user_data = scm_c_eval_string ("<user-data>");
  if (scm_is_true (scm_struct_p (pointer))
      && scm_is_true (scm_equal_p (scm_struct_vtable (pointer), user_data)))
    return scm_struct_ref (pointer, scm_from_int (0));
  else if (SCM_POINTER_P (pointer))
    return pointer;
  return SCM_BOOL_F;
}

wxWindow *
scm_c_get_wx_widget (SCM pointer)
{
  SCM widget = scm_get_wx_widget (pointer);
  if (scm_is_true (widget))
    return static_cast<wxWindow*> (scm_to_pointer (widget));
  return 0;
}

wxBoxSizer *
scm_c_get_wx_box (SCM pointer)
{
  SCM widget = scm_get_wx_widget (pointer);
  if (scm_is_true(widget))
    return static_cast<wxBoxSizer*> (scm_to_pointer (widget));
  return 0;
}

SCM_DEFINE (scm_make_box, "make-box", 2, 0, 0,
           (SCM children, SCM orientation),
           "")
{
  //fprintf (stderr, "wx: make-box\n");
  wxBoxSizer *box = new wxBoxSizer (orientation == scm_from_latin1_symbol ("vertical")
                                    ? wxVERTICAL : wxHORIZONTAL);                                    
  
  for (; !scm_is_null (children); children = scm_cdr (children))
    {
      SCM ui_data = scm_car (children);
      wxBoxSizer *widget = scm_c_get_wx_box (ui_data);
      wxWindow *window_parent = widget->GetContainingWindow ();
      wxBoxSizer *parent = 0;
      if (window_parent)
        parent = static_cast<wxBoxSizer*> (window_parent->GetSizer ());
      if (parent)
        parent->Detach (widget);
      box->Add (widget, 1, wxEXPAND);
    }
  frame->SetSizerAndFit (top_vbox);
  box->Show (true);
  return scm_from_pointer (box, 0);
}

void
set_root_window (wxBoxSizer* r)
{
  // fprintf (stderr, "set_root_window: 0x%x\n", r);
  if (root_window)
    root_container->Detach (root_window);
  root_window = r;
  root_container->Add (root_window, 1, wxEXPAND);
  frame->SetSizerAndFit (top_vbox);
  frame->Show (true);
}

SCM_DEFINE (scm_set_root_window_x, "set-root-window!", 1, 0, 0,
           (SCM ui_data),
           "")
{
  set_root_window (scm_c_get_wx_box (ui_data));
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_insert_char_x, "insert-string!", 2, 0, 0,
            (SCM view, SCM string),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  if (v)
    {
      fprintf (stderr, "wx: insert-string! v=0x%x, %s\n", v, scm_to_utf8_stringn (string, 0));
      v->WriteText (scm_to_utf8_stringn (string, 0));
    }
  return SCM_UNSPECIFIED;
}

SCM_DEFINE (scm_get_substring, "get-substring", 3, 0, 0,
            (SCM view, SCM start, SCM end),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  int s = scm_to_int (start);
  int e = scm_to_int (end);

  int s2 = e > s ? s : e;
  int e2 = e > s ? e : s;
  int  len = e2 - s2;
  //fprintf (stderr, "wx: get-substring: %d, %d [%s]\n", s2, e2, v->GetRangePointer (s2 - 1, len));

  return scm_from_utf8_stringn (v->GetRangePointer (s2 - 1, len), len);
}

SCM_DEFINE (scm_set_text, "set-text", 2, 0, 0,
            (SCM view, SCM string),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  v->SetText (scm_to_utf8_stringn (string, 0));
  char buffer[81];
  strncpy (buffer, scm_to_utf8_stringn (string, 0), 80);
  // fprintf (stderr, "wx: set-text! v=0x%x, %s\n", v, buffer);
  return SCM_UNSPECIFIED;
}

SCM scm_point (SCM);
SCM scm_char_after (SCM, SCM);

SCM_DEFINE (scm_goto_char, "goto-char", 2, 0, 0,
            (SCM view, SCM pos),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  fprintf (stderr, "wx: goto-char %d\n", scm_to_int (pos));
  v->GotoPos (scm_to_int (pos) - 1);
  fprintf (stderr, "p=%d [%c,%d]\n", 1 + v->GetCurrentPos (), scm_to_char (scm_char_after (view, scm_point (view))));
  return pos;
}

SCM_DEFINE (scm_point, "point", 1, 0, 0,
            (SCM view),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  //fprintf (stderr, "wx: point %d\n", 1 + v->GetCurrentPos ());
  return scm_from_int (1 + v->GetCurrentPos ());
}

SCM_DEFINE (scm_point_min, "point-min", 1, 0, 0,
            (SCM view),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  return scm_from_int (1);
}

SCM_DEFINE (scm_point_max, "point-max", 1, 0, 0,
            (SCM view),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  //fprintf (stderr, "wx: point-max %d\n", 1 + v->GetLastPosition ());
  return scm_from_int (1 + v->GetLastPosition ());
}

SCM_DEFINE (scm_set_mark, "set-mark", 2, 0, 0,
            (SCM view, SCM pos),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  //fprintf (stderr, "wx: set-mark %d\n", scm_to_int (pos));
  v->mark_offset = scm_to_int (pos) - 1;
  return pos;
}

SCM_DEFINE (scm_mark, "mark", 1, 0, 0,
            (SCM view),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  //fprintf (stderr, "wx: mark %d\n", 1 + v->mark_offset);
  return scm_from_int (1 + v->mark_offset);
}

SCM_DEFINE (scm_char_after, "char-after", 2, 0, 0,
            (SCM view, SCM pos),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  //fprintf (stderr, "wx: char-after[%d] %c\n", scm_to_int (pos), v->GetCharAt (scm_to_int (pos) - 1));
  return scm_from_char (v->GetCharAt (scm_to_int (pos) - 1));
}

SCM_DEFINE (scm_delete_region_x, "delete-region!", 3, 0, 0,
            (SCM view, SCM start, SCM end),
           "")
{
  Text_view* v = static_cast<Text_view*> (scm_to_pointer (view));
  int s = scm_to_int (start);
  int e = scm_to_int (end);

  int s2 = e > s ? s : e;
  int e2 = e > s ? e : s;
  int len = e2 - s2;

  //fprintf (stderr, "wx: delete-region! %d %d, len=%d\n", s2, e2, len);

  SCM text = scm_from_utf8_string ("");
  if (len > 0)
    {
      text = scm_from_utf8_stringn (v->GetRangePointer (s2 - 1, len), len);
      v->DeleteRange (s2 - 1, len);
    }
  return text;
}

extern "C" {
void scm_register_wx (void);
void scm_init_wx (void);


void
scm_register_wx (void)
{
  scm_c_register_extension ("libguimax",
                            "scm_init_wx",
                            (scm_t_extension_init_func)scm_init_wx, NULL);
}

void
scm_init_wx (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "wx.cc.x"
#endif
}

}
