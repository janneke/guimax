;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor modes)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)

  #:use-module (oop goops)

  #:use-module (emacsy buffer)
  #:use-module (emacsy klecl)
  #:use-module (emacsy core)
  #:use-module (emacsy command)
  #:use-module (emacsy keymap)
  #:use-module (emacsy mode)
  #:use-module (emacsy text)
  #:use-module (emacsy minibuffer)
  #:use-module (emacsy window)

  #:use-module (emacsy core)
  #:export (buffer-substring
            buffer:substring
            current-line
            buffer:current-line
            eval-last-sexp
            guile-interaction-map
            guile-interaction-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; New stuff / Emacsy

(define-interactive (switch-to-buffer-other-window buffer-or-name #:optional norecord)
  (when (= 1 (length (window-list)))
    (split-window))
  (other-window)
  (switch-to-buffer buffer-or-name))

(define-interactive (erase-buffer)
  "Delete the entire contents of the current buffer."
  ;; FIXME: gap-buffer?
  ;; (mark-whole-buffer)
  ;; (delete-region)
  ;;(delete-region (point-min) (1+ (point-max)))
  (delete-region (point-min) (point-max))
  )

(define default-directory (@@ (emacsy minibuffer) default-directory))

(define (absolute-file-name file-name)
  (cond ((string-prefix? "~" file-name) file-name)
        ((string-prefix? "/" file-name) file-name)
        (else (string-append default-directory file-name))))

(define (buffer-substring start end)
  (buffer:substring (current-buffer) start end))

(define-method (buffer:substring (buffer <text-buffer>) start end)
  (let ((s (if (> end start) start end))
        (e (if (> end start) end start)))
    (substring (buffer-string) (1- s) (1- e))))

(define (current-line)
  (buffer:current-line (current-buffer)))

(define-method (buffer:current-line (buffer <text-buffer>))
  (1- (length (string-split (buffer-substring (point-min) (point)) #\newline))))

(define (error-msg-handler key subr msg args . rest)
  (with-output-to-string
    (lambda _
      (display-error #f (current-output-port) subr msg args rest))))

(define* (with-error-msg-handler thunk #:optional (error-msg-handler error-msg-handler))
  (catch #t thunk error-msg-handler))

(define (eval-last-sexp)
  (define (read-file)
    (filter (negate eof-object?) (unfold (lambda _ (eof-object? (peek-char))) read identity (current-input-port))))
  (define (try-read-string string)
    (format (current-error-port) "try-read-string: ~s\n" string)
    (let ((sexp (with-error-msg-handler
                 (lambda _ (with-input-from-string string read-file))
                 (const #f))))
      (warn 'sexp sexp)
      (and (pair? sexp)
           sexp)))
  (let* ((end (point))
         (word-bound (list->char-set '(#\( #\) #\") char-set:whitespace))
         (sexp (let loop ((start (1- (point))))
                 (and (>= start (point-min))
                      (or (and (char-set-contains? word-bound (char-after start))
                               (let ((sexp (try-read-string (buffer-substring start end))))
                                 (and (pair? sexp)
                                      (= (length sexp) 1)
                                      (car sexp))))
                          (loop (1- start)))))))
    (let* ((before (module-map (lambda x x) (current-module)))
           (result (with-error-msg-handler (lambda _ (primitive-eval sexp))))
           (after (module-map (lambda x x) (current-module)))
           (defined (filter (negate (cut member <> before)) after))
           (result (if (pair? defined) (caar defined) result)))
      (message "~s" result)
      result)))

(define guile-interaction-map
  (let ((keymap fundamental-map))
    (for-each
     (match-lambda ((key command) (define-key keymap key command)))
     `(("C-x C-e"   ,eval-last-sexp)))
    keymap))

(define guile-interaction-mode (make <mode> #:mode-name "Guile Interaction" #:mode-map guile-interaction-map))

(define-interactive (list-buffers #:optional arg)
  "Display a list of existing buffers."
  (let ((buffers (buffer-list))
        (current (current-buffer)))
    (define (line->buffer)
      (let ((line (current-line)))
        (and (< line (length buffers))
             (list-ref buffers line))))
    (define buffer-menu-map
      (let ((keymap fundamental-map))
        (for-each
         (match-lambda ((key command) (define-key keymap key command)))
         `(("q"   ,kill-buffer)
           ("RET" ,(lambda _ (switch-to-buffer (list-ref buffers (current-line)))))
           ("o"   ,(lambda _ (and=> (line->buffer) switch-to-buffer-other-window)))
           ("C-o" ,(lambda _ (let ((buffer (current-buffer))
                                   (other (line->buffer)))
                               (when other
                                 ;; (switch-to-buffer-other-window other)
                                 (other-window)
                                 (switch-to-buffer other)
                                 (other-window)
                                 (switch-to-buffer buffer)))))))
        keymap))
    (define buffer-menu-mode (make <mode> #:mode-name "Buffer Menu" #:mode-map buffer-menu-map))
    (define (buffer->entry buffer)
      (string-join `(,(format #f "~a~a~a"
                              (if (eq? buffer current) "." " ")
                              (if (find (compose (cut equal? <> "read-only-mode") mode-name) (buffer-modes buffer))
                                  "%" " ")
                              (if (buffer-modified? buffer) "*" " "))
                     ,(format #f "~21a" (buffer-name buffer))
                     ,(format #f "~6@a" (- (with-buffer buffer (point-max)) 2))
                     ,(format #f "~16a" (if (pair? (buffer-modes buffer)) (or ((compose mode-name car buffer-modes) buffer) "") ""))
                     ,(or (and=> (buffer-file-name buffer) (compose contract-file-name absolute-file-name)) ""))))
    (switch-to-buffer-other-window (new-buffer " *Buffer List*"))
    (set! (buffer-modes (current-buffer)) `(,buffer-menu-mode))
    (erase-buffer)
    (insert (string-join (map buffer->entry buffers) "\n" 'suffix))
    (goto-char (point-min))
    (goto-char 1)
    (set! (buffer-modified-tick (current-buffer)) -1)

    ;; (other-window)
    ;; (switch-to-buffer current)
    (switch-to-buffer-other-window current)
    ;; FIXME
    ;;(redisplay-windows)
    ))

(define-key global-map "C-x C-b" 'list-buffers)
