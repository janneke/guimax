;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax editor text)
  #:use-module (ice-9 optargs)
  #:use-module (oop goops)
  #:use-module (emacsy emacsy)
  #:use-module (emacsy buffer)
  #:use-module (emacsy text)
  #:use-module (emacsy minibuffer)
  #:use-module (emacsy core)
  #:use-module (emacsy window)
  #:use-module (guimax editor interface)
  #:export (<text-view-buffer> text-view-buffer-view))

(define-class <text-view-buffer> (<text-buffer>)
  (view #:accessor buffer-view #:init-form (make-text-view)))

(define text-view-buffer-view buffer-view)

(define (current-view)
  (buffer-view (current-buffer)))

(define-method (buffer:buffer-string (buffer <text-view-buffer>))
  (text-view-get-string (current-view)))

(define-method (buffer:goto-char (buffer <text-view-buffer>) pos)
  (text-view-goto-char (current-view) pos)
  (text-view-point (current-view)))

(define-method (buffer:point (buffer <text-view-buffer>))
  (text-view-point (current-view)))

(define-method (buffer:point-min (buffer <text-view-buffer>))
  (text-view-point-min (current-view)))

(define-method (buffer:point-max (buffer <text-view-buffer>))
  (text-view-point-max (current-view)))

(define-method (buffer:set-mark (buffer <text-view-buffer>) pos)
  (text-view-set-mark (current-view) pos))

(define-method (buffer:mark (buffer <text-view-buffer>))
  (text-view-mark (current-view)))

(define-method (buffer:char-after (buffer <text-view-buffer>) pos)
  (text-view-char-after (current-view) pos))

(define-method (buffer:insert-string (buffer <text-view-buffer>) string)
  (text-view-insert-string! (current-view) string))

(define-method (buffer:insert-char (buffer <text-view-buffer>) char)
  (text-view-insert-char! (current-view) char))

(define-method (buffer:delete-char (buffer <text-view-buffer>) n)
  (let ((beg (point)))
    (forward-char n)
    (delete-region beg (point))))

(define-method (buffer:delete-region (buffer <text-view-buffer>) start end)
  (text-view-delete-region! (current-view) start end))

(define-method (buffer:recenter (buffer <text-view-buffer>) position)
  (text-view-recenter (current-view) position))

(define-public fundamental-mode (make <mode> #:mode-name "fundamental" #:mode-map fundamental-map))

(define-interactive (make-text-buffer #:optional (name "*scratch*"))
  (define (on-enter)
    ;; (format (current-error-port) "Setting text-view to ~a~%" (current-buffer))
    ;; (format (current-error-port) "(selected-window) ~a~%" (selected-window))
    ;; (format (current-error-port) "(user-data) ~a~%" (user-data (selected-window)))
    (let ((ui-data (user-data (selected-window)))
          (buffer (current-buffer)))
      (set! (window-buffer (selected-window)) buffer)
      (when ui-data
        (set-view! ui-data (text-view-buffer-view buffer) #t))))
  (let ((buffer (make <text-view-buffer> #:name name #:buffer-modes `(,fundamental-mode))))
    (add-hook! (buffer-enter-hook buffer) on-enter)
    (add-buffer! buffer)
    buffer))

(module-define! (resolve-module '(emacsy core)) 'make-text-buffer make-text-buffer)
