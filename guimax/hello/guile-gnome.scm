;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax hello guile-gnome)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:use-module (gnome-2)
  #:use-module (gnome glib)
  #:use-module (gnome gtk)
  #:use-module (gnome gtk gdk-event)
  #:use-module (gnome pango)
  #:use-module (guimax hello interface))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle hello))
  ;; ((module-ref hello 'idle))
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (let* ((keyval (gdk-event-key:keyval event))
         (gtk-modifiers (gdk-event-key:modifiers event))
         (unicode (gdk-keyval-to-unicode keyval))
         (name (gdk-keyval-name keyval)))
    (let* ((gdk-key-name->command-char-alist `(("Tab" . #\tab)
                                               ("Return" . #\return)
                                               ("Delete" . #\delete)
                                               ("BackSpace" . #\backspace)
                                               ("Escape" . #\escape)))
           (char (if (not (zero? unicode)) (integer->char unicode)
                     (assoc-ref gdk-key-name->command-char-alist name)))
           (gtk-modifiers->emacsy-alist `((control-mask . control)
                                          (mod1-mask . meta)
                                          (mod4-mask . super)
                                          (shift-mask . shift)))
           (modifiers (map (cut assoc-ref gtk-modifiers->emacsy-alist <>) gtk-modifiers)))
      (%debug "key-press char=~s modifiers=~s\n" char modifiers)
      (and (char? char)
           (let ((char+modifiers (or (assoc-ref '((#\backspace #\delete ())
                                                  (#\delete #\d (control)))
                                                char)
                                     `(,char ,modifiers))))
             ;; (apply (module-ref hello 'key-event) char+modifiers)
             (apply key-event char+modifiers))))))

(define (initialize window hello options)
  (let* ((vbox (make <gtk-vbox>))
         (mode-line (make <gtk-label> #:label " :--- hello"))
         (minibuffer (make <gtk-label> #:label ""))
         (counter (make <gtk-label> #:label "0")))

    (modify-font counter (pango-font-description-from-string "monospace 100"))
    (modify-font minibuffer (pango-font-description-from-string "monospace 30"))
    (modify-font mode-line (pango-font-description-from-string "monospace 30"))

    (add window vbox)
    (pack-start vbox counter #t #t 0)
    (pack-start vbox mode-line #f #f 0)
    (pack-start vbox minibuffer #f #f 0)

    (set-alignment mode-line 0 0)
    (set-alignment minibuffer 0 0)

    (connect window 'key-press-event (gtk-key-press hello))
    (g-timeout-add 100 (gtk-idle hello))

    (show-all window)

    (module-define! hello 'quit (lambda _ (gtk-main-quit)))
    (module-define! hello 'get-counter (lambda _ (string->number (get-text counter))))
    (module-define! hello 'set-counter! (lambda (x) (set-text counter (number->string x))))
    (module-define! hello 'draw-mode-line (lambda (x) (set-text mode-line x)))
    (module-define! hello 'draw-minibuffer (lambda (x) (set-text minibuffer x)))

    (init options)))

(define (run hello options)
  (let ((app (make <gtk-window> #:type 'toplevel)))
    (initialize app hello options)
    (gtk-main)))

(module-define! (resolve-module '(guimax hello interface)) 'run run)
