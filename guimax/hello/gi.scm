;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guimax hello gi)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 match)
  #:use-module (ice-9 receive)
  #:use-module (gi)
  #:use-module (guimax hello interface))

(use-typelibs (("Gio" "2.0") #:prefix gio::)
              ("Gdk" "3.0")
              ("Gtk" "3.0")
              ("GLib" "2.0")
              ("Pango" "1.0"))

(define (%debug . rest)
  (apply format (current-error-port) rest))
(define (%debug . rest)
  #t)

(define ((gtk-idle hello) x)
  (idle)
  #t)

(define (modifier-state->mod-flags modifier-state)
  (filter-map (match-lambda ((mask . modifier)
                             (%debug "mask: ~s, modifier: ~s\n" mask modifier)
                             (and (not (zero? (logand modifier-state mask))) modifier)))
              '((1 . shift)
                (4 . control)
                (8 . meta)
                (64 . super))))

(define ((gtk-key-press hello) widget event)
  (receive (ok keyval)
      (using event (get-keyval))
    (let* ((keymap (keymap:get-default))
           (modifier-state (using keymap (get-modifier-state)))
           (modifiers (modifier-state->mod-flags modifier-state))
           (unicode (and ok keyval (keyval-to-unicode keyval)))
           (name (keyval-name keyval))
           (char (and (number? unicode) (integer->char unicode))))
      (%debug "char: ~s, modifiers=~s\n" char modifiers)
      (and (char? char)
           (let ((char+modifiers (or (assoc-ref `((#\backspace #\delete (,@modifiers))
                                                  (#\delete #\d (control ,@modifiers)))
                                                char)
                                     `(,char ,modifiers))))
             ;; (apply (module-ref hello 'key-event) char+modifiers)
             (apply key-event char+modifiers))))))

(define (activate app hello options)
  (let ((window (cast (application-window:new app) <GtkApplicationWindow>))
        (vbox (cast (box:new ORIENTATION_VERTICAL 0) <GtkBox>))
        (counter (cast (label:new) <GtkLabel>))
        (mode-line (cast (label:new) <GtkLabel>))
        (minibuffer (cast (label:new) <GtkLabel>)))
    (using counter (modify-font (font-description-from-string "monospace 100")))

    (using mode-line
      (modify-font (font-description-from-string "monospace 30"))
      (set-alignment 0 0))

    (using minibuffer
      (modify-font (font-description-from-string "monospace 30"))
      (set-alignment 0 0))

    (using window
      (set-title "Hello Guimax!")
      (set-default-size 200 200)
      (add vbox)
      (connect! key-press-event (gtk-key-press hello)))

    (using vbox
      (pack-start counter #t #t 0)
      (pack-start mode-line #f #f 0)
      (pack-start minibuffer #f #f 0))

    (timeout-add PRIORITY_DEFAULT 100 (gtk-idle hello))

    (using window (show-all))

    (module-define! hello 'quit (lambda _ (using app (quit))))
    (module-define! hello 'get-counter (lambda _  (string->number (using counter (get-text)))))
    (module-define! hello 'set-counter! (lambda (x) (using counter (set-text (number->string x)))))
    (module-define! hello 'draw-mode-line (lambda (x) (using mode-line (set-text x))))
    (module-define! hello 'draw-minibuffer (lambda (x) (using minibuffer (set-text x))))

    ((module-ref hello 'init) options)))

(define (run hello options)
  (let ((app (application:new "org.gtk.example" gio::APPLICATION_FLAGS_NONE))
        (args '()))
    (using app
      (connect! activate (lambda (app) (activate app hello options)))
      (run (length args) args))))

(module-define! (resolve-module '(guimax hello interface)) 'run run)
