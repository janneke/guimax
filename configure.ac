# Guimax --- Guile UI with Emacsy
# Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>

# This file is part of Guimax.
#
# Guimax is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Guimax is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

AC_INIT([Guimax],
        [m4_esyscmd([build-aux/git-version-gen .tarball-version])],
        [guile-user@gnu.org],
        [guimax])
AC_CONFIG_SRCDIR([bin/guimax.in])
AC_CONFIG_AUX_DIR([build-aux])
#AC_CONFIG_MACRO_DIRS([m4])

dnl We use foreign to prevent the useless boilerplate INSTALL
AM_INIT_AUTOMAKE([foreign silent-rules subdir-objects])
AM_SILENT_RULES([yes])
AC_CANONICAL_HOST

AC_PROG_CC_C99
AC_PROG_LIBTOOL

GUILE_PKG([2.2 2.0])
GUILE_PROGS
GUILE_SITE_DIR
GUILE_FLAGS

AC_PATH_PROGS([guile_snarf], [guile-snarf guile-snarf$GUILE_EFFECTIVE_VERSION])
AC_SUBST(guile_snarf)

AC_ARG_VAR([GUILD], [guild (Guile compiler) command])
AS_IF([test "x$GUILD" = "x"],
  [PKG_CHECK_VAR([GUILD], [guile-$GUILE_EFFECTIVE_VERSION], [guild], [],
    [AC_MSG_ERROR(m4_normalize([
      'guild' binary not found; please check your Guile installation.]))])])

dnl Check for Guile-GI.
GUILE_MODULE_AVAILABLE([have_guile_gi], [(gi)])
AM_CONDITIONAL([HAVE_GUILE_GI], [test x$have_guile_gi = xyes])

dnl Check for Guile-Gnome.
GUILE_MODULE_AVAILABLE([have_guile_gnome], [(gnome-2)])
AM_CONDITIONAL([HAVE_GUILE_GNOME], [test x$have_guile_gnome = xyes])

# Check for wxWidgets
WX_CONFIG_OPTIONS
WX_STANDARD_OPTIONS([base,core,html,xrc,stc])
WX_CONVERT_STANDARD_OPTIONS_TO_WXCONFIG_FLAGS

AC_PROG_CXX
AM_OPTIONS_WXCONFIG
WX_CONFIG_CHECK([2.8.0], [have_wx_widgets=yes],,[base,core,html,xrc,stc],[$WXCONFIG_FLAGS])

WX_CFLAGS="$WX_CFLAGS_ONLY"
WX_CXXFLAGS="$WX_CXXFLAGS_ONLY"
AC_SUBST([WX_CPPFLAGS])
AC_SUBST([WX_CXXFLAGS])
AC_SUBST([WX_LIBS])
AM_CONDITIONAL([HAVE_WX_WIDGETS], [test x$have_wx_widgets = xyes])

AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([pre-inst-env:build-aux/pre-inst-env.in],
                [chmod +x pre-inst-env])
AC_CONFIG_FILES([bin/guimax],
                [chmod +x bin/guimax])

AC_OUTPUT
