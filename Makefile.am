# Guimax --- Guile UI with Emacsy
# Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>

# This file is part of Guimax.
#
# Guimax is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Guimax is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

EXTRA_DIST =					\
 .dir-locals.el					\
 HACKING					\
 autogen.sh					\
 build-aux/git-version-gen			\
 build-aux/gitlog-to-changelog			\
 guix.scm

# Modules and scripts
#####################

bin_SCRIPTS =					\
  bin/guimax

EXTRA_DIST += bin/guimax.in

dist_hello_scm_DATA =				\
 guimax/hello/init.scm				\
 guimax/hello/interface.scm

dist_nocompile_hello_scm_DATA =

if HAVE_GUILE_GI
dist_hello_scm_DATA += guimax/hello/gi.scm
else
dist_nocompile_hello_scm_DATA += guimax/hello/gi.scm
endif

if HAVE_GUILE_GNOME
dist_hello_scm_DATA += guimax/hello/guile-gnome.scm
else
dist_nocompile_hello_scm_DATA += guimax/hello/guile-gnome.scm
endif

hello_scmdir = $(GUILE_SITE)/guimax/hello
nocompile_hello_scmdir = $(hello_scmdir)
hello_godir = $(GUILE_SITE_CCACHE)/guimax/hello
hello_go_DATA = $(dist_hello_scm_DATA:%.scm=%.go)

dist_browser_scm_DATA =				\
 guimax/browser/init.scm			\
 guimax/browser/interface.scm			\
 guimax/browser/text.scm			\
 guimax/browser/web.scm				\
 guimax/browser/window.scm

dist_nocompile_browser_scm_DATA =

if HAVE_GUILE_GI
dist_browser_scm_DATA += guimax/browser/gi.scm
else
dist_
dist_nocompile_browser_scm_DATA += guimax/browser/guile-gnome.scm
endif

if HAVE_GUILE_GNOME
dist_browser_scm_DATA += guimax/browser/guile-gnome.scm
else
dist_nocompile_browser_scm_DATA += guimax/browser/guile-gnome.scm
endif

browser_scmdir = $(GUILE_SITE)/guimax/browser
nocompile_browser_scmdir = $(browser_scmdir)
browser_godir = $(GUILE_SITE_CCACHE)/guimax/browser
browser_go_DATA = $(dist_browser_scm_DATA:%.scm=%.go)

dist_editor_scm_DATA =				\
 guimax/editor/init.scm				\
 guimax/editor/interface.scm			\
 guimax/editor/modes.scm			\
 guimax/editor/text.scm				\
 guimax/editor/web.scm				\
 guimax/editor/window.scm

dist_nocompile_editor_scm_DATA =

if HAVE_GUILE_GI
dist_editor_scm_DATA += guimax/editor/gi.scm
else
dist_
dist_nocompile_editor_scm_DATA += guimax/editor/gi.scm
endif

if HAVE_GUILE_GNOME
dist_editor_scm_DATA += guimax/editor/guile-gnome.scm
else
dist_nocompile_editor_scm_DATA += guimax/editor/guile-gnome.scm
endif

if HAVE_WX_WIDGETS
dist_editor_scm_DATA += guimax/editor/wx.scm
else
dist_nocompile_editor_scm_DATA += guimax/editor/wx.scm
endif

editor_scmdir = $(GUILE_SITE)/guimax/editor
nocompile_editor_scmdir = $(editor_scmdir)
editor_godir = $(GUILE_SITE_CCACHE)/guimax/editor
editor_go_DATA = $(dist_editor_scm_DATA:%.scm=%.go)

GUILEC_FLAGS = -Warity-mismatch -Wformat --load-path=$(abs_srcdir)

AM_V_GUILEC = $(AM_V_GUILEC_@AM_V@)
AM_V_GUILEC_ = $(AM_V_GUILEC_@AM_DEFAULT_V@)
AM_V_GUILEC_0 = @echo "  GUILEC" $@;

.scm.go:
	$(AM_V_GUILEC)GUILE_AUTO_COMPILE=0				\
	LTDL_LIBRARY_PATH=$(abs_builddir)/.libs				\
	$(GUILE_TOOLS) compile --target="$(host)" $(GUILEC_FLAGS)	\
	-o "$@" "$<"

%.cc.x: %.cc
	$(AM_V_GEN) $(guile_snarf) -o "$@" $(GUILE_CFLAGS) $(WX_CPPFLAGS) $(AM_CFLAGS) "$<"

guimax/editor/wx/bindings.scm:
	mkdir -p $(@D)
	echo '(define-module (guimax editor wx bindings))' > $@;
	stc=`echo "#include <wx/stc/stc.h>"							\
		| $(CXX) -x c++ -M $(AM_CPPFLAGS) $(WX_CPPFLAGS) -				\
		| grep -o ".*/stc.h" | tee /dev/stderr` ;					\
	grep '#define wxSTC_CMD_' $$stc								\
		| sed 's,#define wxSTC_CMD_\([^ ]\+\) \([0-9]\+\),(define-public STC-\1 \2),'	\
		>> $@

BUILT_SOURCES =
dist_libguimax_wx_la_SOURCES = guimax/editor/wx.cc
if HAVE_WX_WIDGETS
lib_LTLIBRARIES = libguimax-wx.la
libguimax_wx_la_CPPFLAGS = $(WX_CPPFLAGS)
libguimax_wx_la_CXXFLAGS = -fpermissive $(GUILE_CFLAGS) $(WX_CXXFLAGS) $(AM_CFLAGS)
libguimax_wx_la_LIBADD = $(GUILE_LIBS) $(WX_LIBS)

BUILT_SOURCES +=				\
 guimax/editor/wx.cc.x				\
 guimax/editor/wx/bindings.scm
endif

CLEANFILES =					\
 $(BUILT_SOURCES)				\
 $(hello_go_DATA)				\
 $(browser_go_DATA)				\
 $(editor_go_DATA)

# Distribution
##############

# Reproducible tarball
GZIP_ENV = --no-name
am__tar = $${TAR-tar} --sort=name --mtime=@0 --owner=0 --group=0 --numeric-owner -cf - "$$tardir" dist

tag:
	git tag -s v$(PACKAGE_VERSION) -m "$(PACKAGE_NAME) $(PACKAGE_VERSION)."

sign-dist: dist
	gpg --detach-sign $(distdir).tar.gz

dist-hook: gen-ChangeLog
	echo $(VERSION) > $(distdir)/.tarball-version
	git ls-tree -r --name-only HEAD > $(distdir)/.tarball-manifest

.PHONY: gen-ChangeLog
gen-ChangeLog $(distdir)/ChangeLog: config.status
	$(AM_V_GEN)if test -e .git; then		\
	  $(top_srcdir)/build-aux/gitlog-to-changelog	\
	    > $(distdir)/cl-t;				\
	  rm -f $(distdir)/ChangeLog;			\
	  mv $(distdir)/cl-t $(distdir)/ChangeLog;	\
	fi

distcheck-hook:
	set -e;                                 \
	manifest=$(distdir)/.tarball-manifest;  \
	test -f "$$manifest";                   \
	for x in `cat "$$manifest"`;            \
	do                                      \
	    if ! test -f $(distdir)/"$$x"       \
                && ! test "$$x" = .gitignore;   \
	    then                                \
	        echo "Missing: $$x";            \
	        exit 1;                         \
	    fi;                                 \
	done
