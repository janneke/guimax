;;; Guimax --- Guile UI with Emacsy
;;; Copyright (C) 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Guimax.
;;;
;;; Guimax is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Guimax is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Guimax.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; GNU Guix development package.  To build and play, run:
;;
;;   guix environment --ad-hoc -l guix.scm guile
;;
;; To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix environment -l guix.scm
;;
;;; Code:

(use-modules (guix build-system guile)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages)
             (gnu packages)
             (gnu packages gtk)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages perl)
             (gnu packages webkit)
             (gnu packages wxwidgets))

(define %source-dir (dirname (current-filename)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Guile-Gnome
(use-modules (srfi srfi-26)
             (guix download)
             (guix build-system gnu)
             (gnu packages webkit))

(define gtk+? (compose (negate (cut equal? <> "gtk+")) car))
(define-public webkit1gtk/gtk+-2
  (package
    (inherit webkitgtk)
    (name "webkit1gtk")
    (version "2.4.11") ; the last version to support webkit1/gtk+-2
    (source (origin
              (method url-fetch)
              (uri (string-append "https://www.webkitgtk.org/releases/"
                                  "webkitgtk" "-" version ".tar.xz"))
              (sha256
               (base32
                "1xsvnvyvlywwyf6m9ainpsg87jkxjmd37q6zgz9cxb7v3c2ym2jq"))))
    (build-system gnu-build-system)
    (native-inputs `(("flex" ,flex)
                     ("which" ,which)
                     ("libsoup" ,libsoup)
                     ,@(package-native-inputs webkitgtk)))
    (propagated-inputs `(("gtk+" ,gtk+-2)
                         ,@(filter gtk+?
                                   (package-propagated-inputs webkitgtk))))
    (inputs `(("gtk+" ,gtk+-2)
              ,@(filter gtk+? (package-inputs webkitgtk))))
    (outputs '("out"))
    (arguments
     '(#:tests? #f ; no tests
       #:configure-flags '("--with-gtk=2.0"
                           "--enable-webkit1" "--disable-webkit2"
                           "CFLAGS=-fpermissive" "CXXFLAGS=-fpermissive")
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-configure
          (lambda _
            (substitute* "configure"
              (("__GNUC_MINOR__ >= 7") "__GNUC_MINOR__ >= 0"))
            (substitute* "Source/WebCore/bindings/scripts/preprocessor.pm"
              (("/usr/bin/gcc") "gcc"))
            (substitute* "Source/WebCore/dom/make_names.pl"
              (("/usr/bin/cc") "gcc"))
            #t)))))))

(use-modules (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages flex)
             (gnu packages libffi)
             (gnu packages python)
             (gnu packages gettext)
             (gnu packages glib)
             (gnu packages gtk)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages webkit))

(define-public guile-gnome
  (let ((commit "87503235ed2b672f1179675151a017f53a4f8d6f")
        (revision "1"))
    (package
      (name "guile-gnome")
      (version (string-append "2.16.5-" revision "." (string-take commit 7)))
      ;; (source (origin
      ;;           (method url-fetch)
      ;;           (uri
      ;;            (string-append "mirror://gnu/" name
      ;;                           "/guile-gnome-platform/guile-gnome-platform-"
      ;;                           version ".tar.gz"))
      ;;           (patches (search-patches "guile-gnome-add-webkit.patch"))
      ;;           (sha256
      ;;            (base32
      ;;             "1gnf3j96nip5kl99a268i0dy1hj7s1cfs66sps3zwysnkd7qr399"))))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/janneke/guile-gnome.git")
                      (commit commit)))
                (file-name (string-append name "-" version))
                (sha256
                 (base32
                  "1jr33cjv1wfzzqbciyi5kr32kwyjnp564mm1qnphrmjxgbv85yq8"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("bzip2" ,bzip2)
         ("gettext" ,gnu-gettext)
         ("libtool" ,libtool)
         ("pkg-config" ,pkg-config)
         ("python" ,python-2)
         ("texinfo" ,texinfo)
         ("webkit1" ,webkit1gtk/gtk+-2)))
      (inputs `(("atk" ,atk)
                ("gconf" ,gconf)
                ("gdk-pixbuf" ,gdk-pixbuf)
                ("glib" ,glib)
                ("gnome-vfs" ,gnome-vfs)
                ("gobject-introspection" ,gobject-introspection)
                ("gtk+" ,gtk+-2)
                ("gtksourceview" ,gtksourceview-2)
                ("guile" ,guile-2.2)
                ("libffi" ,libffi)
                ("libglade" ,libglade)
                ("libgnome" ,libgnome)
                ("libgnomecanvas" ,libgnomecanvas)
                ("libgnomeui" ,libgnomeui)
                ("mesa" ,mesa)
                ("pango" ,pango)))
      (propagated-inputs
       `(("guile-cairo" ,guile-cairo)
         ("g-wrap" ,g-wrap)
         ("guile-lib" ,guile-lib)))
      (arguments
       `(#:tests? #f                    ;FIXME
         #:phases (modify-phases %standard-phases
                    (replace 'bootstrap ; force auto-update
                      (lambda _
                        (invoke "touch" "build-aux/config.rpath")
                        (invoke "autoreconf" "-vif")))
                    (add-before 'configure 'pre-configure
                      (lambda* (#:key outputs #:allow-other-keys)
                        (let ((out (assoc-ref outputs "out")))
                          (substitute* (find-files "." "^Makefile.in$")
                            (("guilesite :=.*guile/site" all)
                             (string-append all "/@GUILE_EFFECTIVE_VERSION@")))
                          #t))))))
      (native-search-paths
       (list (search-path-specification
              (variable "GUILE_LOAD_PATH")
              (files '("share/guile/site/2.2")))
             (search-path-specification
              (variable "LTDL_LIBRARY_PATH")
              (files '("lib/guile-gnome-2")))))
      (outputs '("out" "debug"))
      (synopsis "Guile wrapper collection for the GNOME core libraries")
      (description
       "Guile-Gnome is a Guile wrapper collection for most of the GNOME core
libraries.  It comes with Gtk+2, Cairo, Glade, GnomeCanvas, Pango, Gtksourceview2, Webkit1, and more.")
      (home-page "https://www.gnu.org/software/guile-gnome/")
      (license license:gpl2+)
      (properties '((upstream-name . "guile-gnome-platform")
                    (ftp-directory . "/gnu/guile-gnome/guile-gnome-platform"))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Guile-GI: in Guix master now
(use-modules (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages gettext)
             (gnu packages glib)
             (gnu packages gtk)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages webkit))

(define-public guile-gi
  (let ((commit "e9d3527c382bae967e650d36882d4e088949049b")
        (revision "5"))
    (package
      (name "guile-gi")
      (version (string-append "0.0.2-" revision "." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/spk121/guile-gi.git")
                      (commit commit)))
                (file-name (string-append name "-" version))
                (sha256
                 (base32
                  "016x0gi8z1lwch64z4saiqyq94cnlf2hj07si4qrnsfk73hnd1wi"))))
      (build-system gnu-build-system)
      (native-inputs `(("autoconf" ,autoconf)
                       ("automake" ,automake)
                       ("gettext" ,gnu-gettext)
                       ("glib-bin" ,glib "bin") ; for glib-compile-resources
                       ("libtool" ,libtool)
                       ("pkg-config" ,pkg-config)
                       ("texinfo" ,texinfo)))
      (propagated-inputs `(("glib" ,glib)
                           ("gobject-introspection" ,gobject-introspection)
                           ("gssettings-desktop-schemas" ,gsettings-desktop-schemas)
                           ("gtk+" ,gtk+)
                           ("gtksourceview" ,gtksourceview)
                           ("guile-lib" ,guile-lib)
                           ("webkitgtk" ,webkitgtk)))
      (inputs `(("guile" ,guile-2.2)))
      (arguments
       `(#:tests? #f
         #:configure-flags '("--with-gnu-filesystem-hierarchy=yes")
         ;;#:make-flags '("XFAIL_TESTS=string/strjoinv.scm gtk_entry/set_alignment.scm gtk_entry/set_alignment_zero.scm")
         #:phases
         (modify-phases %standard-phases
                        (add-after 'unpack 'fixup-test
                                   (lambda _
                                     (substitute* "test/Makefile.am"
                                                  (("atomic_int_set.scm \\\\.*") ""))
                                     #t)))))
      (native-search-paths
       (list (search-path-specification
              (variable "GUILE_LOAD_PATH")
              (files '("share/guile/2.2" "share/guile/site/2.2" "share/guile/site")))
             (search-path-specification
              (variable "GUILE_LOAD_COMPILED_PATH")
              (files '("lib/guile/2.2/site-ccache" "lib/guile/site-ccache")))
             (search-path-specification
              (variable "LTDL_LIBRARY_PATH")
              (files '("lib/guile/2.2/extensions" "lib/guile/2.2" "lib/guile")))
             (search-path-specification
              (variable "LD_LIBRARY_PATH")
              (files '("lib/guile/2.2/extensions" "lib/guile/2.2" "lib/guile" "lib")))))
      (home-page "https://github.com/spk121/guile-gi")
      (synopsis "GObject bindings for Guile")
      (description
       "Guile-GI is a library for Guile that allows using GObject-based
libraries, such as GTK+3.  Its README comes with the disclaimer: This is
pre-alpha code.")
      (license license:gpl3+))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Emacsy
(use-modules (guix build-system gnu)
             (gnu packages autotools)
             (gnu packages compression)
             (gnu packages gettext)
             (gnu packages gl)
             (gnu packages gnome)
             (gnu packages gtk)
             (gnu packages noweb)
             (gnu packages perl)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define-public emacsy
  (let ((commit "5fc62f728331d7bf3d90af79aa7abbe34df283f3")
        (revision "7"))
    (package
      (name "emacsy")
      (version (string-append "0.3-" revision "." (string-take commit 7)))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://gitlab.com/janneke/emacsy.git")
                      (commit commit)))
                (file-name (string-append name "-" version))
                (sha256
                 (base32
                  "04grdqh2lp2sy9dhnmm3pq1knb3q8r467aabq6rn4l2rcvvzmazr"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoconf" ,autoconf)
         ("automake" ,automake)
         ("bzip2" ,bzip2)
         ("guile" ,guile-2.2)
         ("gettext" ,gnu-gettext)
         ("libtool" ,libtool)
         ("perl" ,perl)
         ("pkg-config" ,pkg-config)
         ("texinfo" ,texinfo)
         ;;("texlive" ,texlive)
         ))
      (propagated-inputs
       `(("guile-lib" ,guile-lib)
         ("guile-readline" ,guile-readline)
         ("freeglut" ,freeglut)
         ("gssettings-desktop-schemas" ,gsettings-desktop-schemas)
         ("webkitgtk" ,webkitgtk)))
      (inputs `(("guile" ,guile-2.2)))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-before 'configure 'setenv
             (lambda _
               (setenv "GUILE_AUTO_COMPILE" "0"))))))
      (native-search-paths
       (list (search-path-specification
              (variable "GUILE_LOAD_PATH")
              (files '("share/guile/site/2.2")))
             (search-path-specification
              (variable "GUILE_LOAD_COMPILED_PATH")
              (files '("lib/guile/2.2/site-ccache")))))
      (home-page "https://github.com/shanecelis/emacsy/")
      (synopsis "Embeddable GNU Emacs-like library using Guile")
      (description
       "Emacsy is an embeddable GNU Emacs-like library that uses GNU Guile
as extension language.  Emacsy can give a C program an Emacsy feel with
keymaps, minibuffer, recordable macros, history, tab completion, major
and minor modes, etc., and can also be used as a pure Guile library.  It
comes with a simple counter example using GLUT and browser examples in C
using gtk+-3 and webkitgtk.")
      (license license:gpl3+))))

(use-modules (gnu packages xorg))
(package
  (name "guimax")
  (version "git")
  (source (local-file %source-dir
                      #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system guile-build-system)
  (inputs `(("guile" ,guile-2.2)))
  (native-inputs `(("autoconf" ,autoconf)
                   ("automake" ,automake)
                   ("gettext" ,gnu-gettext)
                   ("libtool" ,libtool)
                   ("perl" ,perl)
                   ("pkg-config" ,pkg-config)
                   ("xorg-server" ,xorg-server)))
  (propagated-inputs `(("emacsy" ,emacsy)
                       ("gtk+" ,gtk+)
                       ("guile-gi" ,guile-gi)
                       ("guile-gnome" ,guile-gnome)
                       ("wxwidgets" ,wxwidgets-3.1)))
  (arguments
   `(#:phases
     (modify-phases %standard-phases
       (add-after 'unpack 'remove-guix.scm
         (lambda _
           (delete-file "guix.scm")
           #t))
       (add-before 'build 'start-xserver
         (lambda* (#:key inputs #:allow-other-keys)
           (let ((xorg-server (assoc-ref inputs "xorg-server"))
                 (mime (assoc-ref inputs "shared-mime-info")))

             ;; There must be a running X server to compile gdk from guile-gnome.
             (system (format #f "~a/bin/Xvfb :1 &" xorg-server))
             (setenv "DISPLAY" ":1")

             ;; The .lang files must be found in $XDG_DATA_HOME/gtksourceview-2.0
             (system "ln -s gtksourceview gtksourceview-2.0")
             (setenv "XDG_DATA_HOME" (getcwd))

             ;; Finally, the mimetypes must be available.
             (setenv "XDG_DATA_DIRS" (string-append mime "/share/")))
           #t))
       )))
  (home-page "https://gitlab.com/janneke/guimax")
  (synopsis "Guile UI with Emacsy")
  (description
   "Guimax is a Guile UI with Emacsy, implementing a simple counter
example and web browser.")
  (license license:gpl3+))
